#include<stdio.h>
void swap(int *a,int *b)
{
int temp;
temp=*a;
*a=*b;
*b=temp;
}
int main()
{
int n1,n2;
printf("enter first number");
scanf("%d",&n1);
printf("enter second number");
scanf("%d",&n2);
printf("value of n1 and n2 before function call %d %d",n1,n2);
swap(&n1,&n2);
printf("value of n1 and n2 after function call %d %d",n1,n2);
return 0;
}
